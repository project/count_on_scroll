/**
 * @file
 * count_on_scroll js.
 */

(function ($) {

  'use strict';

  Drupal.behaviors.count_on_scroll = {
    attach: function (context, settings) {

      // Field setting.
      var duration = drupalSettings.count_on_scroll.counter.duration;

      function testInView($el) {
        var vpH = $(window).height(), // Viewport Height
            st = $(window).scrollTop(), // Scroll Top
            y = $el.offset().top,
            elementHeight = $el.height();
        return ((y < (vpH + st)) && (y > (st - elementHeight)));
      }

      function setInView($element) {
        if (testInView($element)) {
          $element.addClass('inview');
        }
      }

      // Animate numbers on scroll.
      $(window).on('scroll resize load', function() {
        $('.counter').each(function() {
          if (!$(this).hasClass('inview')) {
            setInView($(this));
          }
          else {
            if (!$(this).hasClass('counting')) {
              $(this).addClass('counting');
              startCount($(this));
            }
          }
        });
        if ($('.counter.counting').length == $('.counter').length) {
          $(window).off('scroll resize load');
        }
      });

      function startCount($element) {
        var $this = $element,
            countTo = $this.attr('data-count');

        $({ countNum: $this.text()}).animate({
            countNum: countTo
          },
          {
            duration: parseInt(duration),
            easing: 'swing',
            step: function() {
              $this.text(Math.floor(this.countNum).toLocaleString('en'));
            },
            complete: function() {
              $this.text(Math.floor(this.countNum).toLocaleString('en'));
            }
          });
      }

    }
  };
}(jQuery));
