<?php

namespace Drupal\count_on_scroll\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\NumericFormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'count_on_scroll' formatter.
 *
 * @FieldFormatter(
 *   id = "count_on_scroll_formatter",
 *   label = @Translation("Count on Scroll"),
 *   field_types = {
 *     "integer",
 *   }
 * )
 */
class CountOnScrollFormatter extends NumericFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'duration' => '6000',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    unset($elements['thousand_separator']);
    unset($elements['prefix_suffix']);

    $elements['duration'] = [
      '#type' => 'number',
      '#title' => $this->t('Total duration of the animation.'),
      '#default_value' => $this->getSetting('duration'),
      '#weight' => 50,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();

    if (!empty($settings['duration'])) {
      $summary[] = $this->t('Duration: @duration', ['@duration' => $settings['duration']]);
    }
    else {
      $summary[] = $this->t('Duration: Default');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    $settings = $this->getSettings();

    foreach ($elements as $delta => $element) {
      $elements[$delta]['#prefix'] = '<span class="counter" data-count="' . $elements[$delta]['#markup'] . '">';
      $elements[$delta]['#suffix'] = '</span>';
      $elements[$delta]['#markup'] = 0;
      $elements[$delta]['#attached']['library'][] = 'count_on_scroll/count_on_scroll';
      $elements[$delta]['#attached']['drupalSettings']['count_on_scroll']['counter']['duration'] = $settings['duration'];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  protected function numberFormat($number) {
    return $number;
  }

}
